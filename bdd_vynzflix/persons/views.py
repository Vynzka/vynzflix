from django.shortcuts import render

from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin
from rest_framework.viewsets import ModelViewSet
from rest_framework import viewsets
from rest_framework.response import Response
from persons.models import *
from persons.serializers import *


class RealisateurViewSet(viewsets.ModelViewSet):
    queryset = Realisateur.objects.all()
    serializer_class = AuteurSerializer

class NationaliteViewSet(viewsets.ModelViewSet):
    queryset = Nationalite.objects.all()
    serializer_class = NationaliteSerializer
