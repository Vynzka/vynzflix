from django.db import models

UNKNOWN = 'UNKNOWN'

class Nationalite(models.Model):
	nom = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")
	capitale = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")

	def __str__(self):
		return self.nom

	class Meta:
		verbose_name_plural = "Nationalite"


class Realisateur(models.Model):
	nom = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")
	prenom = models.CharField(
		null=True,
		blank=True,
		max_length=30,
		help_text="Exemple : The Return of the King")
	anniversaire = models.DateField(
		null=False,
		blank=False,
		help_text="Published")
	nationalite = models.ForeignKey(Nationalite, null=True, blank=False, on_delete=models.CASCADE)
	file = models.FileField(
		blank=True,
		help_text="Pick your file")

	def __str__(self):
		return self.prenom + " " + self.nom

	class Meta:
		verbose_name_plural = "Realisateur"
