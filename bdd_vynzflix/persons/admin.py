from django.contrib import admin
from .models import *
# from django.utils.html import format_html


class RealisateurAdmin(admin.ModelAdmin):
    list_display = ('nom', 'prenom', 'anniversaire', 'nationalite')


class NationaliteAdmin(admin.ModelAdmin):
    list_display = ('nom', 'capitale')


admin.site.register(Realisateur, RealisateurAdmin)
admin.site.register(Nationalite, NationaliteAdmin)
