from rest_framework.mixins import UpdateModelMixin
from rest_framework import serializers
from persons.models import *


class NationaliteSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Nationalite
        fields = ('id', 'nom', 'capitale')


class AuteurSerializer(serializers.HyperlinkedModelSerializer):
    nationalite = NationaliteSerializer()

    class Meta:
        model = Realisateur
        fields = ('id', 'nom', 'prenom', 'anniversaire', 'nationalite')
