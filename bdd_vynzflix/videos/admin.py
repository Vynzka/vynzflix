from django.contrib import admin
from .models import *
# from django.utils.html import format_html


class FilmAdmin(admin.ModelAdmin):
    list_display = ('titre', 'extra_titre', 'realisateur', 'style')


class SerieAdmin(admin.ModelAdmin):
    list_display = ('titre', 'extra_titre', 'saison_numero', 'episode_numero', 'style')


class InventoryAdmin(admin.ModelAdmin):
    list_display = ('titre', 'extra_titre', 'realisateur', 'style')


admin.site.register(Inventory, InventoryAdmin)
admin.site.register(Film, FilmAdmin)
admin.site.register(Serie, SerieAdmin)