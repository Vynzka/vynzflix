from django.db import models

# Create your models here.

TYPE = (
	('Comedies', 'Comedies'),
	('Action', 'Action'),
	('Romantic', 'Romantic'),
	('Musical', 'Musical'),
	('Sci-Fi', 'Sci-Fi'),
	('Horror', 'Horror'),
	('Thriller', 'Thriller'),
	('War', 'War'),
)


class Film(models.Model):
	titre = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")
	extra_titre = models.CharField(
		null=True,
		blank=True,
		max_length=30,
		help_text="Exemple : The Return of the King")
	description = models.TextField(
		blank=True,
		max_length=500)
	publication = models.DateField(
		null=False,
		blank=False,
		help_text="Published")
	realisateur = models.ForeignKey('persons.Realisateur', null=True, blank=False, on_delete=models.CASCADE)
	style = models.CharField(
		choices=TYPE,
		max_length=10)
	illustration = models.FileField(
		blank=True,
		help_text="Joindre l'affiche du film : format .png")
	film = models.FileField(
		blank=False,
		null=True,
		help_text="Joindre votre film")

	def __str__(self):
		return self.titre

	class Meta:
		verbose_name_plural = "Films"


class Serie(models.Model):
	saison_numero = models.DecimalField(
		max_digits=2,
		decimal_places=0,
		help_text="Maximum : 99",
	)
	episode_numero = models.DecimalField(
		max_digits=2,
		decimal_places=0,
		help_text="Maximum : 99",
	)
	titre = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : Game of Thrones")
	extra_titre = models.CharField(
		null=True,
		blank=True,
		max_length=30,
		help_text="Exemple : Winter is coming")
	description = models.TextField(
		blank=True,
		max_length=500,
		help_text="Intrigue de l'épisode")
	publication = models.DateField(
		null=False,
		blank=False,
		help_text="Date de publication")
	realisateur = models.ForeignKey('persons.Realisateur', null=True, blank=False, on_delete=models.CASCADE)
	style = models.CharField(
		choices=TYPE,
		max_length=10)
	episode = models.FileField(
		blank=False,
		null=True,
		help_text="Joindre votre episode")

	def __str__(self):
		return self.titre

	class Meta:
		verbose_name_plural = "Series"


class Inventory(models.Model):
	titre = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")
	extra_titre = models.CharField(
		null=True,
		blank=True,
		max_length=30,
		help_text="Exemple : The Return of the King")
	description = models.TextField(
		blank=True,
		max_length=500)
	publication = models.DateField(
		null=False,
		blank=False,
		help_text="Published")
	realisateur = models.ForeignKey('persons.Realisateur', null=True, blank=False, on_delete=models.CASCADE)
	style = models.CharField(
		choices=TYPE,
		max_length=10)
	illustration = models.FileField(
		blank=True,
		help_text="Joindre l'affiche du film : format .png")
	film = models.FileField(
		blank=False,
		null=True,
		help_text="Joindre votre film")

	def __str__(self):
		return self.titre

	class Meta:
		verbose_name_plural = "Inventaire"