from rest_framework.mixins import UpdateModelMixin
from rest_framework import serializers
from videos.models import *


class FilmSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Film
        fields = ('id', 'titre', 'extra_titre', 'description', 'publication', 'realisateur', 'style', 'illustration',
                  'film')


class SerieSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Serie
        fields = ('id', 'titre', 'extra_titre', 'saison_numero', 'episode_numero', 'description', 'publication',
                  'realisateur', 'style', 'episode')


class InventorySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Inventory
        fields = ('id', 'titre', 'extra_titre', 'description', 'publication', 'realisateur', 'style', 'illustration',
                  'film')
