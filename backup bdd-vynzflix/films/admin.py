from django.contrib import admin

from .models import Inventory
# from django.utils.html import format_html

# Register your models here.


class InventoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'extra_name', 'autor', 'style')


admin.site.register(Inventory, InventoryAdmin)
