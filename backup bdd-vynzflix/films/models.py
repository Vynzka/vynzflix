from django.db import models

# Create your models here.

UNKNOWN = 'UNKNOWN'

TYPE = (
	('Comedies', 'Comedies'),
	('Action', 'Action'),
	('Romantic', 'Romantic'),
	('Musical', 'Musical'),
	('Sci-Fi', 'Sci-Fi'),
	('Horror', 'Horror'),
	('Thriller', 'Thriller'),
	('War', 'War'),
)


class Inventory(models.Model):
	name = models.CharField(
		null=False,
		blank=False,
		max_length=30,
		help_text="Exemple : The Lord of the Rings")
	extra_name = models.CharField(
		null=True,
		blank=True,
		max_length=30,
		help_text="Exemple : The Return of the King")
	description = models.TextField(
		blank=True,
		max_length=500)
	date = models.DateField(
		null=False,
		blank=False,
		help_text="Published")
	autor = models.CharField(
		null=False,
		blank=False,
		max_length=30)
	style = models.CharField(
		choices=TYPE,
		max_length=10)
	file = models.FileField(
		blank=True,
		help_text="Pick your file")

	def __str__(self):
		return self.name

	class Meta:
		verbose_name_plural = "Inventory"
