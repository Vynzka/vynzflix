# Generated by Django 2.2.1 on 2019-07-18 17:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('films', '0003_auto_20190701_2108'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='inventory',
            options={'verbose_name_plural': 'Inventory'},
        ),
    ]
