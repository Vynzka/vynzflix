from rest_framework.mixins import UpdateModelMixin
from rest_framework import serializers
from films.models import *

import string

class InventorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Inventory
        fields = ('id', 'name', 'extra_name', 'description', 'date', 'autor', 'style', 'file')

